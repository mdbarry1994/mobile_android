package entities

data class Flashcard(var front: String, var back: String){
    fun returnFlasCard(): ArrayList<Flashcard>{
        var flashcard_1=Flashcard("US","Washington")
        var flashcard_2=Flashcard("Guinea","Conakry")
        var flashcard_3=Flashcard("Senegal","Dakar")
        var flashcard_4=Flashcard("France","Paris")
        var flashcard_5=Flashcard("Germany","Berlin")
        var flashcard_6=Flashcard("Spain","Madrid")
        var flashcard_7=Flashcard("Japan","Tokyo")
        var flashcard_8=Flashcard("Saudi Arabia","Riyad")
        var flashcard_9=Flashcard("China","Pekin")
        var flashcard_10=Flashcard("Mali","Bamako")
        var flascardArray= arrayListOf<Flashcard>()
        flascardArray.add(flashcard_1)
        flascardArray.add(flashcard_2)
        flascardArray.add(flashcard_3)
        flascardArray.add(flashcard_4)
        flascardArray.add(flashcard_5)
        flascardArray.add(flashcard_6)
        flascardArray.add(flashcard_7)
        flascardArray.add(flashcard_8)
        flascardArray.add(flashcard_9)
        flascardArray.add(flashcard_10)

        return flascardArray

    }

}
